package th.co.ktb.next.archetype.model.request;

import lombok.Builder;
import th.co.ktb.next.common.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
* This is a sample model for request of an API.
* 1. The request model must implement the BaseRequest interface.
* 2. This class serves as input of the respective API.
* */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestMessage extends BaseRequest {

    private String message;
    private String topic;
}
